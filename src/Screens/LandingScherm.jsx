import React from 'react'
import { CstTekst } from '../Cst'

const { LandingScherm: LandingTxt } = CstTekst
const LandingScherm = () => (
  <React.Fragment>
    <h1>{LandingTxt.Titel}</h1>
    <hr />
    <h4>
      {LandingTxt.Uitleg}
    </h4>
  </React.Fragment>
)

export default LandingScherm
