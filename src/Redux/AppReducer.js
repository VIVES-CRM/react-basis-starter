import { Actions } from '../Cst'

export const InitialState = {
  Fout: false,
  Foutmelding: null,
}


export const AppReducer = (state = InitialState, action) => {
  switch (action.type) {
    // TODO: verwijder DummyFout
    case Actions.DummyFout:
      return {
        ...state,
        Fout: true,
        Foutmelding: Actions.Foutmelding,
      }

    default:
      return state
  }
}
