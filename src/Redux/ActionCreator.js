/* eslint camelcase: "off" */
import { Actions, CstTekst } from '../Cst'
import { } from '../Api/Api'

const { Foutmeldingen, OnbekendeFout } = CstTekst

export const DummyActie = () => (
  {
    type: Actions.DummyFout,
    Foutmelding: OnbekendeFout,
    FoutDetails: Foutmeldingen.ApiOnbereikbaar,
  })

export const Dummy2 = () => { }
